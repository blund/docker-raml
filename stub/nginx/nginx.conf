worker_processes auto;
error_log /var/log/nginx/error.log error;
daemon on;

events {
  worker_connections 32;
}

http {
    types {
        text/html                             html htm shtml;
        text/css                              css;
        text/xml                              xml;
        image/gif                             gif;
        image/jpeg                            jpeg jpg;
        application/x-javascript              js;
    }
    etag off;
    access_log /var/log/nginx/access.log;
    server_names_hash_bucket_size  64;
    keepalive_timeout   70;
    default_type text/html;

    server {
        listen              80;
        listen              443 ssl;
        ssl_certificate     /data/nginx/server.crt;
        ssl_certificate_key /data/nginx/server.key;

        location /api/ {
            # Proxy to the RAML server.
            proxy_pass http://127.0.0.1:4000/;
        }

        location / {
            # Proxy to the app server.
            # This IP must be your own local IP or to a local socat tunnel.
            proxy_pass http://127.0.0.1:4201/;
        }

    }
}
