# RAML environment
Build this image and then use it to create and serve your mock API.

This image we use for:
    1. Seed a RAML project for a specific purpose.
    2. Serve as a proxy to route traffic to RAML service running inside
        container and to app server running outside container.

This makes it so we can run our app and our mock api on the same domain:port
but still using two different servers, a nodejs raml server for the api and
a `ng serve` or similar to serve the app.

This docker dir has a Makefile to assist you in building the image and then
running a container to seed a new project.

The seeded project dir has it's own Makefile to assist you when managing your project.

## Build image
Build the docker image as:
```
make build
```

## Usage
1.  Seed a new RAML project.
Do this from your empty project directory once to seed a new project.
It will populate the directory with stub files.
```
make seed DIR=/opt/myproject
```

2. Manage and run project.
Do this when working on or serving your RAML api.
Run the following from your project root folder.

```
make run
make serve RAML=example.yaml
```

Edit `nginx.conf` to reflect your local IP and preferences.
Make sure your app server is bound to 0.0.0.0, not just 127.0.0.1.

In some cases we must tunnel traffic so that it looks to the app server as coming from local:
```sh
make tunnel ADDR=192.160.0.1:4200
```

Test it from outside container.
```sh
curl -k https://CONTAINER-IP/resource
```

## HTTPS/SSL/TLS
The nginx server listens to http on port 80 and https on port 443.
All traffic is proxeid to the RAML server.
in the seeded dir there is a directory named nginx with the ssl
certificate files. Update these (and restart container) if you
change the servername from `api.server`.
Note: create cert files using `make cert`.

## Help
The container could show some help:
```
make help

```

The seeded `Makefile` also has a `help` target to show some info.
