FROM alpine:latest
MAINTAINER http://gitlab.com/blund

RUN mkdir /data \
    && chmod 777 /data
RUN apk update
RUN apk add bash git curl nodejs nodejs-npm socat
RUN apk add nginx
RUN npm install -g osprey-mock-service

ENV PATH "$PATH:/root"
ADD ./tools/. /root
ADD ./stub /root/stub
CMD ["help"]
