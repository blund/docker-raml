# Copyright Blockie AB, 2016.
# Version: 0.9

IMAGE = registry.gitlab.com/blund/docker-raml

help:
	@echo Usage:
	@echo "make build              Builds the image."
	@echo "make seed DIR=dir       Seeds the project into DIR."

build:
	docker build -t $(IMAGE) .

seed:
	@bash -c 'if [[ "$(DIR)" == "" || ! -d "$(DIR)" ]]; then echo "Error: missing/non existing DIR"; exit 1; fi; docker run -ti --rm -v $(DIR):/data $(IMAGE) seed'

.PHONY: help build seed
